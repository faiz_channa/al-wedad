<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner15.png);"></section>
<main id="main">
	<div class="map-holder">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14948644.59494812!2d36.03957103039324!3d23.833834106364563!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15e7b33fe7952a41%3A0x5960504bc21ab69b!2sSaudi%20Arabia!5e0!3m2!1sen!2s!4v1578929471216!5m2!1sen!2s" width="600" height="450" frameborder="0" allowfullscreen=""></iframe>
		<div class="location-details">
			<strong class="title">عن الفرع</strong>
			<p>العنوان: شارع التيب الليبن بحجوار اتغيبل ١٢٣٤٥</p>
			<a href="tel:246-794-6230" class="tel">246-794-6230</a>
			<strong class="title">ساعات العمل</strong>
			<p>٩ صباحا - ٦ مساء</p>
		</div>		
	</div>
</main>

<?php include('footer.php'); ?>	