<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner2.png);"></section>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav id="sidebar">
					<ul>
						<li><a href="#">عن الجمعية</a></li>
						<li><a href="#">الرؤية والرسالة</a></li>
						<li><a href="#" class="active">مجلس الإدارة</a></li>
						<li><a href="#">اهدافنا</a></li>
						<li><a href="#">رسالة الرئيس</a></li>
						<li><a href="#">الجمعية العمومية</a></li>
					</ul>
				</nav>
				<div id="content">
					<div class="row bod-section">
						<div class="col-lg-3 col-md-3 col-sm-4 col-12 column">
							<div class="img-holder"><img src="images/img-bod1.png" alt="image"></div>
							<h3>أ. يوسف عبدالرحمن محمد ولي</h3>
							<strong class="sub-title">رئيس مجلس الإدارة </strong>
							<p>مهندس زراعي ومؤسس لمجموعة شركات امات في السعودية ومصر والسودان والإمارات ومؤسس جمعية الوداد الخيرية</p>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-4 col-12 column">
							<div class="img-holder"><img src="images/img-bod2.png" alt="image"></div>
							<h3>د. سمير عبد الوهاب بحراوي</h3>
							<strong class="sub-title">رئيس مجلس الإدارة </strong>
							<p>مهندس زراعي ومؤسس لمجموعة شركات امات في السعودية ومصر والسودان والإمارات ومؤسس جمعية الوداد الخيرية</p>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-4 col-12 column">
							<div class="img-holder"><img src="images/img-bod3.png" alt="image"></div>
							<h3>أ. محمد عبد الرحمن محمد مؤمنة</h3>
							<strong class="sub-title">رئيس مجلس الإدارة </strong>
							<p>مهندس زراعي ومؤسس لمجموعة شركات امات في السعودية ومصر والسودان والإمارات ومؤسس جمعية الوداد الخيرية</p>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-4 col-12 column">
							<div class="img-holder"><img src="images/img-bod4.png" alt="image"></div>
							<h3>م. حسين سعيد بحري</h3>
							<strong class="sub-title">رئيس مجلس الإدارة </strong>
							<p>مهندس زراعي ومؤسس لمجموعة شركات امات في السعودية ومصر والسودان والإمارات ومؤسس جمعية الوداد الخيرية</p>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-4 col-12 column">
							<div class="img-holder"><img src="images/img-bod1.png" alt="image"></div>
							<h3>أ. يوسف عبدالرحمن محمد ولي</h3>
							<strong class="sub-title">رئيس مجلس الإدارة </strong>
							<p>مهندس زراعي ومؤسس لمجموعة شركات امات في السعودية ومصر والسودان والإمارات ومؤسس جمعية الوداد الخيرية</p>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-4 col-12 column">
							<div class="img-holder"><img src="images/img-bod2.png" alt="image"></div>
							<h3>د. سمير عبد الوهاب بحراوي</h3>
							<strong class="sub-title">رئيس مجلس الإدارة </strong>
							<p>مهندس زراعي ومؤسس لمجموعة شركات امات في السعودية ومصر والسودان والإمارات ومؤسس جمعية الوداد الخيرية</p>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-4 col-12 column">
							<div class="img-holder"><img src="images/img-bod3.png" alt="image"></div>
							<h3>أ. محمد عبد الرحمن محمد مؤمنة</h3>
							<strong class="sub-title">رئيس مجلس الإدارة </strong>
							<p>مهندس زراعي ومؤسس لمجموعة شركات امات في السعودية ومصر والسودان والإمارات ومؤسس جمعية الوداد الخيرية</p>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-4 col-12 column">
							<div class="img-holder"><img src="images/img-bod4.png" alt="image"></div>
							<h3>م. حسين سعيد بحري</h3>
							<strong class="sub-title">رئيس مجلس الإدارة </strong>
							<p>مهندس زراعي ومؤسس لمجموعة شركات امات في السعودية ومصر والسودان والإمارات ومؤسس جمعية الوداد الخيرية</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php include('footer.php'); ?>	