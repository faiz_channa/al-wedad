<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner9.png);"></section>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col-12 content-section">
				<ul class="nav-tabs">
					<li><a href="#">الإيـواء</a></li>
					<li><a href="#">الإحتضــــــــان</a></li>
					<li><a href="#" class="active">المـتابعـــــة</a></li>
					<li><a href="#">توعية المجتمع</a></li>
				</ul>
				<div class="holder">
					<div class="align-right"><img src="images/img7.png" alt="image description"></div>
					<div class="description">
						<h2>المـتابعـــــة</h2>
						<p>نحن في جمعية الوداد الأهلية نهتم بشؤون أطفال الجمعية حتى بعد توفير أسر مناسبة لهم تحتضنهم، وذلك لنضمن أن يجدوا الرعاية المناسبة لهم بشكل متواصل. ومن أجل تحقيق هذا الهدف، نحن نقوم بعمل زيارات دورية لسكن جميع الأسر الحاضنة لجميع أطفال الجمعية، ويقوم بهذه المهمة أخصائيات اجتماعيات على مستوى عالٍ من الحرفية والمهنية.</p>
						<p>
تقوم الجمعية بتطبيق معايير علمية مدروسة ومتطورة لمتابعة نمو ونشأة الأطفال الأيتام لدى أسرهم المحتضنة.
</p>
						<p>تسعى جمعية الوداد لتكوين مجتمع مصغر تحت إسـم "الوداد" يسوده الود والتعاون والترابط بين الأسر المحتضنة.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php include('footer.php'); ?>	