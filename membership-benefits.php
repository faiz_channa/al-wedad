<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner5.png);"></section>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav id="sidebar">
					<ul>
						<li><a href="#">عن الجمعية</a></li>
						<li><a href="#">الرؤية والرسالة</a></li>
						<li><a href="#">مجلس الإدارة</a></li>
						<li><a href="#">اهدافنا</a></li>
						<li><a href="#">رسالة الرئيس</a></li>
						<li><a href="#" class="active">الجمعية العمومية</a></li>
					</ul>
				</nav>
				<div id="content">
					<ul class="nav-tabs">
						<li><a href="#">أنواع العضويات</a></li>
						<li><a href="#">استمارة الطلب</a></li>
						<li><a href="#">شروط العضوية</a></li>
						<li><a href="#" class="active">مميزات العضوية</a></li>
					</ul>
					<div class="text-description">
						<h2>مميزات العضوية</h2>
						<ul>
							<li>.حضور اجتماعات الجمعية العمومية ومناقشة موضوعاتها والتصويت على القرارات والترشيح لعضوية مجلس الادارة (للعضو العامل ).</li>
							<li>2.تقديم برامج وفعاليات (للعضو العامل والمنتسب )</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php include('footer.php'); ?>	