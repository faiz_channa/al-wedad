<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner11.png);"></section>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav id="sidebar">
					<ul>
						<li><a href="#" class="active">أنواع التبرع</a></li>
						<li><a href="#">طرق الدفع</a></li>
					</ul>
				</nav>
				<div id="content">
					<div class="row donations-section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-12 column">
							<h3>زكاة المال</h3>
							<div class="img-holder"><img src="images/img8.png" alt="image"></div>
							<p>ينشأ ما يقرب ٥٠٠ طفل مجهول الأبوين في السعودية سنويا دون أسرة محبة
لق‭ ‬اعتمادة‭ ‬التشفي‭ ‬لتراجع‭ ‬الترتيب‭ ‬بسرع‭ ‬قويات‭ ‬واءا‭ ‬خلائحة‭ ‬بها‭ ‬كونك‭ ‬إلى‭ ‬موات‭ ‬ومن‭ ‬إضافة‭ ‬بشكل‭ ‬الرس‭. ‬إصدارك</p>
							<a href="#" class="btn btn-primary green">تبــــــــرع</a>						
</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-12 column">
							<h3>زكاة الخير</h3>
							<div class="img-holder"><img src="images/img9.png" alt="image"></div>
							<p>ينشأ ما يقرب ٥٠٠ طفل مجهول الأبوين في السعودية سنويا دون أسرة محبة
لق‭ ‬اعتمادة‭ ‬التشفي‭ ‬لتراجع‭ ‬الترتيب‭ ‬بسرع‭ ‬قويات‭ ‬واءا‭ ‬خلائحة‭ ‬بها‭ ‬كونك‭ ‬إلى‭ ‬موات‭ ‬ومن‭ ‬إضافة‭ ‬بشكل‭ ‬الرس‭. ‬إصدارك</p>
							<a href="#" class="btn btn-primary green">تبــــــــرع</a>						
</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-12 column">
							<h3>طلب إحتضان</h3>
							<div class="img-holder"><img src="images/img10.png" alt="image"></div>
							<p>ينشأ ما يقرب ٥٠٠ طفل مجهول الأبوين في السعودية سنويا دون أسرة محبة
لق‭ ‬اعتمادة‭ ‬التشفي‭ ‬لتراجع‭ ‬الترتيب‭ ‬بسرع‭ ‬قويات‭ ‬واءا‭ ‬خلائحة‭ ‬بها‭ ‬كونك‭ ‬إلى‭ ‬موات‭ ‬ومن‭ ‬إضافة‭ ‬بشكل‭ ‬الرس‭. ‬إصدارك</p>
							<a href="#" class="btn btn-primary green">تقدم بالطلب</a>						
</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php include('footer.php'); ?>	