<?php include('header.php'); ?>

<div class="visual">
	<section class="main-gallery">
		<div>
			<img src="images/img-banner.png" alt="image carousel">
			<div class="gallery-title">
				<div class="holder">
					<h2>احتضان مجهولي الأبوين في أسر محبة</h2>
				</div>
			</div>
		</div>
		<div>
			<img src="images/img-banner.png" alt="image carousel">
			<div class="gallery-title">
				<div class="holder">
					<h2>احتضان مجهولي الأبوين في أسر محبة</h2>
				</div>
			</div>
		</div>
	</section>
	<div class="quotes-gallery">
		<div class="slide">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 hidden-md"></div>
					<div class="col-lg-7 col-md-12">
						<div class="logo"><img src="images/logo-partner5.png" alt="image carousel"></div>
						<div class="text">
							<strong class="title">جمعية الوداد هى الجهة الحصرية المعتمدة</strong>
							<p>من وزارة العمل والتنمية الإجتماعية باستلام جميع الأطفال مجهولي الأبوين في السعودية</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="slide">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 hidden-md"></div>
					<div class="col-lg-7 col-md-12">
						<div class="logo"><img src="images/logo-partner5.png" alt="image carousel"></div>
						<div class="text">
							<strong class="title">جمعية الوداد هى الجهة الحصرية المعتمدة</strong>
							<p>من وزارة العمل والتنمية الإجتماعية باستلام جميع الأطفال مجهولي الأبوين في السعودية</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="slide">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 hidden-md"></div>
					<div class="col-lg-7 col-md-12">
						<div class="logo"><img src="images/logo-partner5.png" alt="image carousel"></div>
						<div class="text">
							<strong class="title">جمعية الوداد هى الجهة الحصرية المعتمدة</strong>
							<p>من وزارة العمل والتنمية الإجتماعية باستلام جميع الأطفال مجهولي الأبوين في السعودية</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<section class="intro-section">
	<div class="row">
		<div class="col-md-6 col-sm-6 col-12 column">
			<img src="images/img1.png" alt="image" class="img-fluid">
		</div>
		<div class="align-self-end col-md-6 col-sm-6 col-12 column description">
			<div class="text">
				<h1>احتضان بالرضاعة يتوافق مع الشرع</h1>
				<strong class="sub-title">تهدف جمعية الوداد الى ايجاد أسر منتقاة لاحتضان 100% من مجهولي الأبوين في السعودية</strong>
			</div>
			<div class="know-more">
				<p>نحن في جمعية الوداد الخيرية نسعى إلى القيام بدور فعال في بناء مجتمعات إسلامية تسودها الرحمة والتكافل الاجتماعي، وذلك من خلال الاهتمام بقضايا الأطفال مجهولي الأبوين، ومد يد العون لهم حتى يصبحوا أفرادا منتجين وصالحين في المجتمع.</p>
				<a href="#" class="btn btn-primary">تعرف اكثر</a>
			</div>
		</div>
	</div>
</section>

<section class="video-section">
	<div class="holder">
		<div class="description">
			<p>جمعية الوداد هي الجهة الحصرية المعتمدة من وزارة العمل و التنمية الاجتماعية باستلام جميع الأطفال مجهولي الأبوين في السعودية</p>
			<h3> تطوع معنا وكن أحد أصدقاء #جمعية_الوداد ،،،</h3>
			<a href="#" class="btn btn-default">تعرف اكثر</a>
		</div>
		<div class="video-block">
			<img src="images/img-video.png" alt="video image">
			<a href="#" class="btn-play"></a>
		</div>
	</div>
</section>

<section class="partners-section">
	<div class="holder">
		<h2>شركاء النجاح</h2>
		<div class="partners-gallery">
			<div>
				<div class="logo-holder"><img src="images/logo-partner1.png" alt="image description"></div>
			</div>
			<div>
				<div class="logo-holder"><img src="images/logo-partner2.png" alt="image description"></div>
			</div>
			<div>
				<div class="logo-holder"><img src="images/logo-partner3.png" alt="image description"></div>
			</div>
			<div>
				<div class="logo-holder"><img src="images/logo-partner4.png" alt="image description"></div>
			</div>
			<div>
				<div class="logo-holder"><img src="images/logo-partner1.png" alt="image description"></div>
			</div>
			<div>
				<div class="logo-holder"><img src="images/logo-partner2.png" alt="image description"></div>
			</div>
		</div>
	</div>
</section>

<section class="donate-section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2>تبرع الآن بالطرق المختلفة</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-4 col-12 column">
				<div class="ico"><img src="images/ico1.png" alt="image description"></div>
				<h3>عن طريق تحويل بنكي</h3>
				<h3 class="sub-title">Bank Transfer</h3>
			</div>
			<div class="col-md-4 col-sm-4 col-12 column">
				<div class="ico"><img src="images/ico2.png" alt="image description"></div>
				<h3>عن طريق كارت إئتمان</h3>
				<h3 class="sub-title">Credit Card</h3>
			</div>
			<div class="col-md-4 col-sm-4 col-12 column">
				<div class="ico"><img src="images/ico3.png" alt="image description"></div>
				<h3>عن طريق رسالة نصية</h3>
				<h3 class="sub-title">SMS</h3>
			</div>
		</div>
	</div>
</section>

<?php include('footer.php'); ?>	