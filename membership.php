<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner3.png);"></section>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav id="sidebar">
					<ul>
						<li><a href="#">عن الجمعية</a></li>
						<li><a href="#">الرؤية والرسالة</a></li>
						<li><a href="#">مجلس الإدارة</a></li>
						<li><a href="#">اهدافنا</a></li>
						<li><a href="#">رسالة الرئيس</a></li>
						<li><a href="#" class="active">الجمعية العمومية</a></li>
					</ul>
				</nav>
				<div id="content">
					<ul class="nav-tabs">
						<li><a href="#" class="active">أنواع العضويات</a></li>
						<li><a href="#">استمارة الطلب</a></li>
						<li><a href="#">شروط العضوية</a></li>
						<li><a href="#">مميزات العضوية</a></li>
					</ul>
					<div class="align-left"><img src="images/img5.png" alt="image description"></div>
					<div class="description">
						<div class="text-block">
							<h2>عضو عامل (1200ريال سنويا )</h2>
							<p>حضور إجتماعات الجمعية العمومية ومناقشة موضوعاتها والتصويت على القرارات والترشيح لعضوية مجلس الإدارة </p>
						</div>
						<div class="text-block">
							<h2>عضو منتسب (120ريال سنويا )</h2>
							<p>المشاركة في تقديم البرامج والفعاليات الخاصة بالجمعية</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php include('footer.php'); ?>	