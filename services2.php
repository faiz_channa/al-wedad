<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner8.png);"></section>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col-12 content-section">
				<ul class="nav-tabs">
					<li><a href="#">الإيـواء</a></li>
					<li><a href="#">الإحتضــــــــان</a></li>
					<li><a href="#" class="active">المـتابعـــــة</a></li>
					<li><a href="#">توعية المجتمع</a></li>
				</ul>
				<div class="holder">
					<div class="align-right"><img src="images/img6.png" alt="image description"></div>
					<div class="description">
						<h2>الإحتضــــــــان</h2>
						<p> إختيار أسر حاضنة مناسبة بإشتراط الرضاعة بهدف :</p>
						<ul class="list">
							<li>تحقيق التربية السليمة داخل أسرة طبيعية .</li>
							<li>تحقيق التواجد الشرعي لليتيم بعد سن البلوغ .</li>
							<li>تحقيق أول خطوات تغيير نظرة المجتمع .</li>
						</ul>
						<p>قد أثبتت جميع الدراسات الميدانية، على المستوى العالمي، وليس في المملكة فحسب، أن البيئة المثالية لنشأة أي طفل لا تكون إلا في بيئة أسرية طبيعية، وأن دُور رعاية الأطفال تكون أبعد ما تكون عن البيئة المثالية لنشأة الأطفال. ومن هذا المنطلق، فنحن في جمعية الوداد الأهلية لا نسعى للاحتفاظ بالأطفال لفترة طويلة كما هو الحال مع الجمعيات الأخرى، ولكننا نقوم باختيار أسر مناسبة للحضانة أطفال الجمعية في أقرب فرصة ممكنة من بعد دخول الطفل تحت رعايتنا، وذلك من أجل توفير بيئة أسرية طبيعية دائمة لهم.</p>
						<p>ونقوم باختيار الأسر الحاضنة بعد استيفاءئها لعدد كبير من المتطلبات، الأمر الذي يضمن اختيار أكثر الأسر المتقدمة ملاءمة لتلبية احتياجات كل طفل من أطفال الجمعية. أهم هذه المتطلبات هي:</p>
						<ul class="list">
							<li>تقييم أفراد الأسرة المتقدمة من الناحية الاجتماعية والنفسية.</li>
							<li>تقييم مسكن الأسرة المتقدمة من حيث ملاءمته لاستقبال فرد إضافي في الأسرة.</li>
							<li>أن لا يكون دخل الأسرة أقل من 8000 ريـال شهريا.</li>
							<li>أن تكون الأم سعوديين.</li>
							<li>أن لا يكون عمر الأم أكثر من 50 سنة.</li>
						</ul>
						<p>ومن أجل توفير حياة طبيعية لأطفال الجمعية عند الأسر الحاضنة لهم بعد بلوغ الأطفال لسن البلوغ، فنحن نشترط الإرضاع للطفل، وبذلك يصبح للطفل أم وأب وإخوان من الرضاعة، فلا يضطر الطفل، سواء كان ذكرا أم أنثى، إلى الانعزال عن بعض أفراد الأسرة بعد سن البلوغ.</p>
						<h2 class="heading-green">هدف الإحتضان</h2>
						<p>نعمل على تشجيع الإحتضان بشرط الإرضاع بأسلوب يحقق الهدف المنشود بتربية الطفل في بيئة صالحة عن طريق حسن إختيار الأسرة الحاضنة والتي تتوفر فيها الشروط اللازمة مع حسن المتابعة بعد التنسيق مع وزارة العمل والتنمية الاجتماعية وتعطي الجمعية أسلوب الإحتضان الأولوية على أساليب الرعاية الأخرى .</p>
						<p>الطفل مخلوق حساس وقابل للتأثر والتشكيل من خلال المؤثرات المحيطة به لذلك له حاجات أساسية </p>
						<ul class="list">
							<li>تأمين حاجته من الغذاء والعناية الصحية .</li>
							<li>تأمين الأمان والدفئ الأسري من خلال اجواء عائلية  مستقرة .</li>
							<li>إشباع حاجته في الإدراك والمعرفة عن طريق الرعاية والتفهم والتعليم .</li>
						</ul>
						<h2 class="heading-green">ضوابط شروط الإحتضان</h2>
						<ul class="list">
							<li>أن تكون الأسرة سعودية.</li>
							<li>أن تكون الأسرة مكونة من زوجين وأن لا يتجاوز سن الزوجة عن خمسين عاماً ، ويجوز عند الضرورة رعايته من إمرأة فقط .</li>
							<li>أن يثبت الكشف الطبي خلو الأسرة من الأمراض السارية المعدية .</li>
							<li>أن يراعى عدم وجود فرق واضح بين بشرة الطفل ولون بشرة أطفال الأسرة الحاضنة .</li>
							<li>التحقق من حسن سيرة وسلوك الأسرة . </li>
							<li>أن يثبت البحث الإجتماعي صلاحية الأسرة لرعاية الطفل اجتماعياً ونفسياً واقتصادياً.</li>
							<li>إرضاع الطفل من قبل الحاضنة أو أحد أقارب الزوجة أو الزوج في حال كون الأسرة غير منجبة .</li>
						</ul>
						<h2 class="heading-green">متطلبات الإحتضان</h2>
						<ul class="list">
							<li>تسجيل طلب إحتضان جديد من خلال الموقع الإلكتروني للجمعية WWW.WEDAD.ORG</li>
							<li>الأصل والصورة لكرت العائلة .</li>
							<li>الأصل والصورة من هوية الزوج – هوية الزوجة – هوية المرضعة .</li>
							<li>خطاب تعريف للراتب للزوجين وكشف حساب بنكي لآخر ثلاث شهور.</li>
							<li>صورة من ملكية المنزل أو عقد الإيجار .</li>
							<li>صورة من آخر مؤهل علمي للزوجين .</li>
							<li>كروكي لمنزل الأسرة .</li>
							<li>الكشف الطبي والتقييم النفسي للزوج والزوجة .</li>
							<li>خطاب تعريفي من عمدة الحي .</li>
							<li>تعبئة جميع النماذج الخاصة بطلب الإحتضان .</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php include('footer.php'); ?>	