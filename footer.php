    <ul class="social-sticky">
        <li><a href="#" class="fa fa-facebook-f"></a></li>
        <li><a href="#" class="fa fa-twitter"></a></li>
        <li><a href="#" class="fa fa-youtube"></a></li>
        <li><a href="#" class="fa fa-instagram"></a></li>
        <li><a href="#" class="fa fa-share-alt"></a></li>
    </ul>
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form class="newsletter-from">
                        <div class="holder">
                            <div class="column">
                                <h3>اشترك في نشرة اخبارنا</h3>
                            </div>
                            <div class="column add">
                                <input type="text" class="form-control" placeholder="الإسم">
                            </div>
                            <div class="column add">
                                <input type="email" class="form-control" placeholder="البريد الألكتروني">
                            </div>
                            <div class="column">
                                <input type="submit" value="اشترك" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                    <div class="footer-holder">
                        <div class="tweets-block"><img src="images/img-tweets.png" alt="tweets"></div>
                        <div class="footer-nav">
                            <div class="map-box"><img src="images/img-map.png" alt="tweets"></div>
                            <ul>
                                <li><a href="#" class="title">عن الجمعية</a></li>
                                <li><a href="#">عن الجمعية</a></li>
                                <li><a href="#">الرؤية والرسالة</a></li>
                                <li><a href="#">مجلس الإدارة</a></li>
                                <li><a href="#">اهدافنا</a></li>
                                <li><a href="#"> رسالة الرئيس</a></li>
                                <li><a href="#">الجمعية العمومية</a></li>
                            </ul>
                            <ul>
                                <li><a href="#" class="title">خدماتنا</a></li>
                                <li><a href="#">خدماتنا</a></li>
                            </ul>
                            <ul>
                                <li><a href="#" class="title">تبرع لليتيم</a></li>
                                <li><a href="#">أنواع التبرع</a></li>
                                <li><a href="#">طرق الدفع</a></li>
                            </ul>
                            <ul>
                                <li><a href="#" class="title">شركاء النجاح</a></li>
                            </ul>
                            <ul>
                                <li><a href="#" class="title">المركز الإعلامي</a></li>
                                <li><a href="#">اخر اخبارنا</a></li>
                                <li><a href="#">البوم الصور</a></li>
                                <li><a href="#">الفيديو</a></li>
                            </ul>
                            <ul>
                                <li><a href="#" class="title">تطوع معنا</a></li>
                                <li><a href="#" class="title">الوظائف</a></li>
                                <li><a href="#" class="title">اتصل بنا</a></li>
                                <li><a href="#" class="title">فروعنا</a></li>
                            </ul>
                        </div>
                        <div class="copyright-section row">
                            <div class="col-md-6 col-sm-6 col-12">جميع الحقوق محفوظة جمعية الوداد الخيرية ٢٠١٨</div>
                            <div class="col-md-6 col-sm-6 col-12">سياسة الخصوصية - الشروط والأحكام</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="js/jquery-2.1.3.min.js" rel="stylesheet" type="text/javascript"></script>
<script src="js/bootstrap.min.js" rel="stylesheet" type="text/javascript"></script>
<script src="js/bootstrap.bundle.min.js" rel="stylesheet" type="text/javascript"></script>
<script type="text/javascript" src="js/slick.min.js"></script>
<!--<script type="text/javascript" src="js/jquery.imgzoom-min.js"></script>-->
<script type="text/javascript" src="js/jquery.fancybox.js"></script>
<script src="js/script.js" type="text/javascript"></script>
</body>
</html>