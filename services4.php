<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner10.png);"></section>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col-12 content-section">
				<ul class="nav-tabs">
					<li><a href="#">الإيـواء</a></li>
					<li><a href="#">الإحتضــــــــان</a></li>
					<li><a href="#">المـتابعـــــة</a></li>
					<li><a href="#" class="active">توعية المجتمع</a></li>
				</ul>
				<div class="holder">
					<div class="align-right"><img src="images/img7.png" alt="image description"></div>
					<div class="description">
						<h2>توعية المجتمع</h2>
						<ul class="list">
							<li>مد أيادي بيضاء لبراءة ضالة ليس لها ذنب يحمِّلها المجتمع أخطاء لم يرتكبوها.</li>
							<li>العمل على مستوى وقائي للحد من انتشار الظاهرة .</li>
							<li>تخفيف الظاهرة عبر نشر الوعي بمخاطرها بين كل فئات المجتمع والدعوة للفضيلة ومحاربة الرذيلة وتغيير اتجاهات المجتمع نحو الأطفال الذين لا ذنب لهم.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php include('footer.php'); ?>	