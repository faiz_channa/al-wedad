<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner4.png);"></section>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav id="sidebar">
					<ul>
						<li><a href="#">عن الجمعية</a></li>
						<li><a href="#">الرؤية والرسالة</a></li>
						<li><a href="#">مجلس الإدارة</a></li>
						<li><a href="#">اهدافنا</a></li>
						<li><a href="#">رسالة الرئيس</a></li>
						<li><a href="#" class="active">الجمعية العمومية</a></li>
					</ul>
				</nav>
				<div id="content">
					<ul class="nav-tabs">
						<li><a href="#">أنواع العضويات</a></li>
						<li><a href="#">استمارة الطلب</a></li>
						<li><a href="#" class="active">شروط العضوية</a></li>
						<li><a href="#">مميزات العضوية</a></li>
					</ul>
					<div class="text-description">
						<h2>شروط العضوية </h2>
						<ul>
							<li>. أن يكون سعودي الجنسية </li>
							<li>2. أن يكون قد أتم السنة الثامنة عشر من عمره . (باستثناء العضو المنتسب )</li>
							<li>3. أن يكون كامل الأهلية المعتبرة شرعاً.</li>
							<li>4. أن يكون غير محكوم عليه بإدانة في جريمة مخلة بالشرف أو الأمانة مالم يكون قد رد اعتباره إليه .</li>
							<li>5. أن يكون قد سدد الحد الأدنى من الاشتراك السنوي . (حسب نوع العضوية )</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php include('footer.php'); ?>	