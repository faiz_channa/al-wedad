<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner7.png);"></section>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav id="sidebar">
					<ul>
						<li><a href="#">عن الجمعية</a></li>
						<li><a href="#">الرؤية والرسالة</a></li>
						<li><a href="#">مجلس الإدارة</a></li>
						<li><a href="#">اهدافنا</a></li>
						<li><a href="#">رسالة الرئيس</a></li>
						<li><a href="#" class="active">الجمعية العمومية</a></li>
					</ul>
				</nav>
				<div id="content">
					<ul class="nav-tabs">
						<li><a href="#">أنواع العضويات</a></li>
						<li><a href="#">استمارة الطلب</a></li>
						<li><a href="#" class="active">شروط العضوية</a></li>
						<li><a href="#">مميزات العضوية</a></li>
					</ul>
					<form action="#" class="volunter-form text-description">
						<h2>استمارة طلب العضوية الجمعية العمومية</h2>
						<p>البيانات الشخصية</p>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الاسم رباعياً" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="مكان الميلاد" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="تاريخ الميلاد" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="رقم بطاقة الأحوال" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="مصدرها" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="تاريخها" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 column">
								<p>المؤهل العلمي</p>
								<div class="radio-fields">
									<label for="radio1">
										<input id="radio1" type="radio" name="radio" checked>
										<span class="fake-input"></span>
										<span class="fake-label">دكتوراه</span>
									</label>
									<label for="radio2">
										<input id="radio2" type="radio" name="radio">
										<span class="fake-input"></span>
										<span class="fake-label">ماجستير</span>
									</label>
									<label for="radio3">
										<input id="radio3" type="radio" name="radio">
										<span class="fake-input"></span>
										<span class="fake-label">جامعي</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 column">
								<p>السكن</p>
								<input type="text" placeholder="العنوان" class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="المدينة" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الحي" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الشارع" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="هاتف المنزل" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12">
								<p>العمل</p>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="جهة العمل" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الوظيفة" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12">
								<p>بيانات عامة</p>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="ص.ب" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الجوال" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="المدينة" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الرمز البريدي" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="البريد الإلكتروني" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 column">
								<p>نوع العضوية المطلوبة</p>
								<div class="radio-fields">
									<label for="radio4">
										<input id="radio4" type="radio" name="radio1" checked>
										<span class="fake-input"></span>
										<span class="fake-label">دكتوراه</span>
									</label>
									<label for="radio5">
										<input id="radio5" type="radio" name="radio1">
										<span class="fake-input"></span>
										<span class="fake-label">ماجستير</span>
									</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="مبلغ الاشتراك" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="طريقة السداد" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="تاريخ الطلب" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column">
								<input type="submit" value="تسجيل" class="btn btn-primary">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</main>

<?php include('footer.php'); ?>	