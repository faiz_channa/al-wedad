<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner11.png);"></section>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav id="sidebar">
					<ul>
						<li><a href="#">أنواع التبرع</a></li>
						<li><a href="#" class="active">طرق الدفع</a></li>
					</ul>
				</nav>
				<div id="content" class="donations-payment">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-12 column">
							<div class="ico"><img src="images/ico3.png" alt="image description"></div>
							<strong class="heading">من خلال رسالة <span>SMS</span></strong>
							<strong class="sub-heading">ارسال رسالة على <span>٥٠٧٩</span></strong>
						</div>
						<div class="col-md-4 col-sm-4 col-12 column">
							<div class="ico"><img src="images/ico2.png" alt="image description"></div>
							<strong class="heading">من خلال الموقع الالكتروني للجمعية <br>باستخدام البطاقات الائتمانية.</strong>
						</div>
						<div class="col-md-4 col-sm-4 col-12 column">
							<div class="ico"><img src="images/ico1.png" alt="image description"></div>
							<strong class="heading">التبرع من خلال حسابات الجمعية البنكية.</strong>
							<ul>
								<li>حساب رقم:  <span>302620122</span></li>
								<li>رمز السويفت: <span>EBILAEAD</span></li>
								<li>رمز التوجيه (Routing Code): <span>302620122</span></li>
								<li>بنك الجزيرة</li>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<a href="#" class="btn btn-primary green">تبــــــــرع</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php include('footer.php'); ?>	