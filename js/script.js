$(document).ready(function(){
    $('.main-gallery').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots:false,
        autoplay: true,
        //infinite: false,
        nextArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
        prevArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
    });
    $('.quotes-gallery').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots:true,
        autoplay: true,
        arrows: false,
    });
    $('.partners-gallery').slick({
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        nextArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
        prevArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.popover-dismiss').popover({
        trigger: 'focus'
    });
    $('.btn-play').on('click', function(e){
        e.preventDefault();
        $('.video-block img').hide();
        $(this).hide();
        $(".video-block").append('<iframe width="200" height="100" src="https://www.youtube.com/embed/zoMYU_nOGNg?rel=0&amp;showinfo=0&amp;enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen=""></iframe>');
    });

    $('.nav-opener').click(function(e){
        e.preventDefault();
        $('body').addClass('menu-active');
    });
    $('.nav-close').click(function(e){
        e.preventDefault();
        $('body').removeClass('menu-active');
    });
});

//      Equal Height JS Function
equalheight = function(container){
    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $(container).each(function() {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}
$(window).load(function() {
    equalheight('.eqHeightEd ');
});
$(window).resize(function(){
    equalheight('.eqHeightEd');
});







