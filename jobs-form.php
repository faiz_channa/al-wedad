<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner13.png);"></section>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav id="sidebar">
					<ul>
						<li><a href="#">العمل مع الجمعية</a></li>
						<li><a href="#" class="active">الوظائف المتاحة</a></li>
					</ul>
				</nav>
				<div id="content">
				<form action="#" class="volunter-form text-description">
						<h2>وظيفة ١ </h2>
						<p>رجاء ادخال البيانات الآتية</p>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الإسم الأول" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الإسم الثاني" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 column"><input type="text" placeholder="البريد الألكتروني" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="تاريخ الميلاد" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الدولة" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="المدينة" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الوظيفة" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الجنس" class="form-control"></div>
						</div> 
						<div class="row">
							<div class="col-12 column"><input type="text" placeholder="العنوان" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="رقم الهاتف" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="رقم الجوال" class="form-control"></div>
						</div> 
						<br>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column">
								<input type="submit" value="تقدم" class="btn btn-primary">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-12">
								<img src="images/img-social.png" alt="image" class="img-fluid">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</main>

<?php include('footer.php'); ?>	