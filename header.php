<?php
if(isset($_GET['lang']) and !empty($_GET['lang'])){
	$lang = $_SEESION['lang']=$_GET['lang'];
}else{
	$lang = $_SEESION['lang']='eng';
}
?>
<html lang="en" <?php if ($lang=='arb') {echo 'dir="rtl" ';} ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" type="image/png" href="images/favicon.png">
	<link rel="shortcut icon" type="image/png" href="images/favicon.ico">
	<title>Al Waleda</title>
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&display=swap" rel="stylesheet">
	<link href="css/<?php echo ($lang=='arb' ? 'rtl' : 'ltr'); ?>.css" rel="stylesheet" type="text/css" media="all">
</head>
<body class="<?php echo $lang; ?>">
	<!--<div class="pp"></div>-->
	<div id="wrapper">
		<header id="header">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="holder">
							<a href="#" class="nav-opener">Menu</a>
							<div class="logo"><a href="#"><img src="images/logo.png" alt="Al Waleda"></a></div>
							<div class="contact-info">
								<div class="tel"><a href="tel:920011334 ">920011334</a></div>
								<div class="btn-holder">
									<a href="#" class="btn btn-primary orange">طلب إحتضان</a>
									<a href="#" class="btn btn-primary green">تبــــــــرع</a>
								</div>
							</div>
							<div class="left-panel">
								<div class="waranty-logo"><img src="images/banner-wranty.png" alt="10 years waranty"></div>
								<ul class="brands">
									<li><a href="#"><img src="images/logo-vision.png" alt="image"></a></li>
									<li><a href="#"><img src="images/logo-tamiz.png" alt="image"></a></li>
								</ul>
								<ul class="info">
									<li><a href="#" class="contact">اتصل بنا</a></li>
									<li><a href="#" class="btn-search">search</a></li>
									<li><a href="#" class="lang">EN</a></li>
								</ul>
							</div>
						</div>
						<nav id="nav">
							<a href="#" class="nav-close">Close</a>
							<div class="contact-info">
								<div class="tel"><a href="tel:920011334 ">920011334</a></div>
								<div class="btn-holder">
									<a href="#" class="btn btn-primary orange">طلب إحتضان</a>
									<a href="#" class="btn btn-primary green">تبــــــــرع</a>
								</div>
							</div>
							<ul>
								<li><a class="active" href="#">عن الجمعية</a>
									<ul>
										<li><a href="#">عن الجمعية</a></li>
										<li><a href="#">الرؤية والرسالة</a></li>
										<li><a href="#">مجلس الإدارة</a></li>
										<li><a href="#">اهدافنا</a></li>
										<li><a href="#">رسالة الرئيس</a></li>
										<li><a href="#">الجمعية العمومية</a></li>
									</ul>
								</li>
								<li><a href="#">خدماتنا</a></li>
								<li><a href="#">تبرع لليتيم</a></li>
								<li><a href="#">شركاء النجاح</a></li>
								<li><a href="#">تطوع معنا</a></li>
								<li><a href="#">المركز الإعلامي</a></li>
								<li><a href="#">فروعنا</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>