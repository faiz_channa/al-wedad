<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner15.png);"></section>
<main id="main">
	<div class="map-holder add">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14948644.59494812!2d36.03957103039324!3d23.833834106364563!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15e7b33fe7952a41%3A0x5960504bc21ab69b!2sSaudi%20Arabia!5e0!3m2!1sen!2s!4v1578929471216!5m2!1sen!2s" width="600" height="450" frameborder="0" allowfullscreen=""></iframe>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="contact-section">
					<h2>مواقعنا</h2>
					<div class="row contact-details">
						<div class="col-lg-3 col-md-3 col-sm-3 col-12 column">
							<h3>الرئيسي</h3>
							<p>العنوان شارع التيب الليبن. االلل. بحجوار اتغيبل ١٢٣٤٥</p>
							<span class="tel">246-794-6230</span>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-12 column">
							<h3>الفرعى</h3>
							<p>العنوان شارع التيب الليبن. االلل. بحجوار اتغيبل ١٢٣٤٥</p>
							<span class="tel">246-794-6230</span>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-12 column">
							<h3>الفرعى</h3>
							<p>العنوان شارع التيب الليبن. االلل. بحجوار اتغيبل ١٢٣٤٥</p>
							<span class="tel">246-794-6230</span>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-12 column">
							<h3>تواصل معنا</h3>
							<div class="email">
								<a href="#">info@wedadcharity.com</a>
							</div>
							<span class="tel1">920011334</span>
							<ul class="social-networks">
								<li><a href="#" class="fa fa-instagram"></a></li>
								<li><a href="#" class="fa fa-youtube"></a></li>
								<li><a href="#" class="fa fa-twitter"></a></li>
								<li><a href="#" class="fa fa-facebook-f"></a></li>
							</ul>
						</div>
					</div>
					<form action="#" class="contact-form">
						<h2>إرسال رسالة</h2>
						<p>إملا الإستمارة أدناه</p>
						<br>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الإسم الأول" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder=" الجوال" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الرقم السري" class="form-control"></div>
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="الشركة" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column"><input type="text" placeholder="البلد" class="form-control"></div>
						</div>
						<div class="row">
							<div class="col-12 column"><textarea rows="5" cols="5" placeholder="تعليقات" class="form-control"></textarea></div>
						</div>
						<div class="row">
							<div class="col-12 column"><img src="images/img-captcha.png" alt="image description" class="img-fluid"></div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-6 col-md-6 col-lg-6 column">
								<input type="submit" value="إرسال" class="btn btn-primary green">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</main>

<?php include('footer.php'); ?>	