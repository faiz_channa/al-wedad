<?php include('header.php'); ?>

<section class="banner" style="background-image:url(images/img-banner14.png);"></section>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav id="sidebar">
					<ul>
						<li><a href="#">الأخبار</a></li>
						<li><a href="#" class="active">ألبوم الصور</a></li>
						<li><a href="#">الفيديو</a></li>
					</ul>
				</nav>
				<div id="content">
					<div class="row media-gallery">
						<div class="col-lg-4 col-md-4 col-sm-4 col-12 column">
							<a href="images/img17.png" rel="lightbox1">
								<img src="images/img17.png" alt="image">
								<div class="caption">
									<strong class="title">آلبوم ١</strong>
									<span class="number">12</span>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-12 column">
							<a href="images/img18.png" rel="lightbox1">
								<img src="images/img18.png" alt="image">
								<div class="caption">
									<strong class="title">آلبوم ١</strong>
									<span class="number">12</span>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-12 column">
							<a href="images/img19.png" rel="lightbox1">
								<img src="images/img19.png" alt="image">
								<div class="caption">
									<strong class="title">آلبوم ١</strong>
									<span class="number">12</span>
								</div>
							</a>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-12 column">
							<a href="images/img20.png" rel="lightbox1">
								<img src="images/img20.png" alt="image">
								<div class="caption">
									<strong class="title">آلبوم ١</strong>
									<span class="number">12</span>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-12 column">
							<a href="images/img21.png" rel="lightbox1">
								<img src="images/img21.png" alt="image">
								<div class="caption">
									<strong class="title">آلبوم ١</strong>
									<span class="number">12</span>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-12 column">
							<a href="images/img22.png" rel="lightbox1">
								<img src="images/img22.png" alt="image">
								<div class="caption">
									<strong class="title">آلبوم ١</strong>
									<span class="number">12</span>
								</div>
							</a>
						</div>

						<div class="col-lg-4 col-md-4 col-sm-4 col-12 column">
							<a href="images/img23.png" rel="lightbox1">
								<img src="images/img23.png" alt="image">
								<div class="caption">
									<strong class="title">آلبوم ١</strong>
									<span class="number">12</span>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-12 column">
							<a href="images/img24.png" rel="lightbox1">
								<img src="images/img24.png" alt="image">
								<div class="caption">
									<strong class="title">آلبوم ١</strong>
									<span class="number">12</span>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-12 column">
							<a href="images/img25.png" rel="lightbox1">
								<img src="images/img25.png" alt="image">
								<div class="caption">
									<strong class="title">آلبوم ١</strong>
									<span class="number">12</span>
								</div>
							</a>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php include('footer.php'); ?>	